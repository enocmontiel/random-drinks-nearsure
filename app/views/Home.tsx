import React, { Component } from "react";
import {
	StyleSheet,
	View,
	ScrollView,
	ActivityIndicator,
	Text,
	RefreshControl
} from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";
import Drinks, { Drink } from "../services/drinks";
import Card from "../components/Card";

interface HomeProps {}

interface HomeState {
	loading: boolean;
	errorMessage: string;
	hasError: boolean;
	drinksData: Drink[];
}

class Home extends Component<HomeProps, HomeState> {
	static navigationOptions = {
		headerTitle: "Random Drinks",
		headerStyle: {
			backgroundColor: "#00C0CD"
		},
		headerTintColor: "#fff"
	};
	_onRefresh = () => {
		this.setState({ loading: true, hasError: false });
		this.getData();
	};
	getData = () => {
		Drinks.getDrinks()
			.then(response => {
				this.setState({
					drinksData: response.drinks,
					loading: false
				});
			})
			.catch(err => {
				this.setState({
					hasError: true,
					loading: false
				});
			});
	};
	constructor(props: HomeProps) {
		super(props);

		this.state = {
			loading: true,
			errorMessage:
				"Error on getting the drink list, please pull to refresh the content.",
			hasError: false,
			drinksData: []
		};
		this.getData();
	}
	render() {
		return this.state.loading ? (
			<View style={styles.loaderContainer}>
				<ActivityIndicator size='large' color='#fff' />
			</View>
		) : (
			<ScrollView
				style={styles.container}
				refreshControl={
					<RefreshControl
						style={{ backgroundColor: "transparent" }}
						tintColor='#fff'
						refreshing={this.state.loading}
						onRefresh={this._onRefresh}
					/>
				}
				contentContainerStyle={styles.contentScroll}
			>
				{!this.state.hasError ? (
					this.state.drinksData.map(drink => {
						return (
							<Card
								key={drink.idDrink}
								title={drink.strDrink}
								image={drink.strDrinkThumb}
							/>
						);
					})
				) : (
					<View style={styles.contentError}>
						<Text style={styles.errorText}>{this.state.errorMessage}</Text>
					</View>
				)}
			</ScrollView>
		);
	}
}

const HomeNavigation = createStackNavigator({
	Home: {
		screen: Home
	}
});

export default createAppContainer(HomeNavigation);

const styles = StyleSheet.create({
	container: {
		backgroundColor: "#00C0CD",
		padding: 15
	},
	safeArea: {
		backgroundColor: "#00C0CD"
	},
	mainContent: {
		height: "100%",
		justifyContent: "center",
		alignItems: "center"
	},
	contentScroll: {
		justifyContent: "center",
		paddingBottom: 40
	},
	loaderContainer: {
		flexGrow: 1,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#00C0CD"
	},
	contentError: {
		flex: 1
	},
	errorText: {
		fontSize: 18,
		fontWeight: "bold",
		textAlign: "center",
		flex: 1,
		color: "#fff"
	}
});
