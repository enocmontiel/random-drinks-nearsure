import React, { Component } from "react";
import { StyleSheet, Text, View, Image, ActivityIndicator } from "react-native";

interface CardProps {
	title?: string;
	image?: string;
}

interface CardState {
	loadingImage: boolean;
}

class Card extends Component<CardProps, CardState> {
	constructor(props: CardProps) {
		super(props);

		this.state = {
			loadingImage: true
		};
	}
	render() {
		return (
			<View style={styles.cardContent}>
				<Text style={styles.title}>{this.props.title}</Text>
				{this.state.loadingImage ? (
					<ActivityIndicator style={styles.loader} size='small' color='#000' />
				) : null}
				<Image
					style={styles.image}
					source={{ uri: this.props.image }}
					onLoadEnd={() => this.setState({ loadingImage: false })}
				/>
			</View>
		);
	}
}

export default Card;

const styles = StyleSheet.create({
	cardContent: {
		height: 150,
		marginBottom: 10,
		padding: 15,
		flexDirection: "row",
		justifyContent: "space-between",
		backgroundColor: "#fff",
		borderRadius: 5,
		shadowOffset: { width: 1, height: 3 },
		shadowColor: "black",
		shadowOpacity: 0.4
	},
	title: {
		fontSize: 24,
		color: "#717171",
		flex: 1,
		flexWrap: "wrap",
		marginRight: 4
	},
	loaderImage: {
		height: 100,
		width: 100,
		alignItems: "center",
		justifyContent: "center"
	},
	image: {
		height: "100%",
		width: 100
	},
	loader: {
		height: "100%",
		width: 100,
		position: "absolute",
		right: 15,
		top: 15,
		zIndex: 1,
		backgroundColor: "#fff"
	}
});
