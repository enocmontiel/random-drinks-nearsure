export interface Drink {
	strDrink: string;
	strDrinkThumb: string;
	idDrink: string;
}

export interface ResponseDrinks {
	drinks: Drink[];
}

class Drinks {
	getDrinks() {
		return fetch(
			"https://www.thecocktaildb.com/api/json/v1/1/filter.php?g=Cocktail_glass"
		).then(response => {
			if (!response.ok) {
				throw new Error(response.statusText);
			}
			return response.json() as Promise<ResponseDrinks>;
		});
	}
}

export default new Drinks();
