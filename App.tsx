import React, { Component } from "react";
import HomeNavigation from "./app/views/Home";

interface Props {}
export default class App extends Component<Props> {
	render() {
		return <HomeNavigation />;
	}
}
